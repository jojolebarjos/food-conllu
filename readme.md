
# Cooking Recipes dependencies

This corpus covers food recipes, in English.


## License/Copyright

Samples have been randomly selected from [Food.com](https://www.food.com/), under the following [Terms of Use](http://www.scrippsnetworksinteractive.com/terms-of-use).

This work follows the same rational as Benikova et al. in "NoSta-D Named Entity Annotation for German": Guidelines and Dataset", which state:

_A sentence, as compared to a full document, is a comparably short snippet of text and not regarded as a complete work. It can be literally cited, if the source of the sentence is clearly stated and the sentence was, intentionally and for an extended period of time, available to the public. (...) the original articles are neither complete nor reconstructable, thus the copyright of the articles as a whole is not violated. According to American copyright law, there is no copyright on a common phrase, which is taken out of its context._

As such, this dataset is released to public domain, under CC-BY-SA 4.0.

Annotations have been created with the help of [UD Annotatrix](https://github.com/jonorthwash/ud-annotatrix).


## Design choices

 * Annotation policy roughly follows *UD_English-EWT*.
   * In particular, symbols are always split (e.g. hyphen and slashes). Therefore, "1/2 cup" is four tokens.
   * Symbolic tokens are usually either SYM or PUNCT, depending on whether is represent an actual word.
     * "-" is SYM when it represents a range (i.e. "to"), PUNCT otherwise.
     * "/" is SYM in units (e.g. per hour), PUNCT otherwise (e.g. alternative, fraction). Note that many treebanks (incl. EWT) have inconsistencies for this symbol.
     * "&" is CCONJ, to conform to EWT.
     * Units are SYM (e.g. "°", """).
     * Unit modifiers (e.g. Celsius, [Fahrenheit](https://en.wiktionary.org/wiki/Fahrenheit#English)) are adjectives.
 * When processing, be sure to normalize [degree](https://en.wiktionary.org/wiki/%C2%B0#Translingual) symbols (e.g. replace [similar looking](https://en.wiktionary.org/wiki/º) characters). Also note that Unicode has dedicated codepoint for degrees ([°C](https://en.wiktionary.org/wiki/%C2%B0C), [°F](https://en.wiktionary.org/wiki/%C2%B0F#Translingual)).


## TODO

 * ";" is used almost as a sentence separator, and not as side note marker. Maybe tokenizer should split it as a new sentence instead of using parataxis?
 * "hamburger-style" is annotated as a compound, but
   * EWT defines "hamburger" as a obl:npmod
   * EWT defines "style" as an ADJ
 * need to be coherent with VERB used as adjectives (i.e. amod vs acl)
 * parataxis vs appos for long parenthesis extension
 * confirm what are "room temperature", "ice cold", "to taste"...
 * ambiguity between amod and compound for ADJ in very common structures (e.g. green bell pepper, white cake)
 * properly use obl:npmod
 * shall we use [extended syntax](https://universaldependencies.org/u/overview/enhanced-syntax.html#propagation-of-conjuncts) to flatten a bit the conjuncts?
 * add "newpar"?
